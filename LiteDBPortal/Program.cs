﻿
using Avalonia;
using Avalonia.ReactiveUI;

namespace LiteDBPortal
{
    class Program
    {
        public static void Main(string[] args) => BuildAvaloniaApp() 
            .StartWithClassicDesktopLifetime(args);

        public static AppBuilder BuildAvaloniaApp() 
            => AppBuilder.Configure<App>()
                .UseReactiveUI()
                .UsePlatformDetect();
    }
}
