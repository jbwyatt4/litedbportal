using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Controls.Ribbon;
using Avalonia.Markup.Xaml;
using LiteDBPortal.Generator;
using LiteDBPortal.Services;
using LiteDBPortal.ViewModels;
using MessageBox.Avalonia;
using MessageBox.Avalonia.Enums;

namespace LiteDBPortal.Views
{
    public class MainWindow : Window
    {
        private RibbonComboButton _dbCloseButton;
        private RibbonComboButton _dbExportButton;
        private RibbonComboButton _dbImportButton;
        public MainWindow()
        {
            InitializeComponent();
            InitializeCallbacks();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        private void InitializeCallbacks()
        {
            _dbCloseButton = this.FindControl<RibbonComboButton>("OnCloseDbSelection");
            _dbCloseButton.SelectionChanged += OnDbCloseButtonOnSelectionChanged;

            _dbExportButton = this.FindControl<RibbonComboButton>("OnExportDatabase");
            _dbExportButton.SelectionChanged += OnDbExportButtonOnSelectionChanged;
            
            _dbImportButton = this.FindControl<RibbonComboButton>("OnImportToDatabase");
            _dbImportButton.SelectionChanged += OnDbImportButtonOnSelectionChanged;
        }
        
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            var vm = DataContext as MainWindowViewModel;
            vm?.DatabaseBrowserViewModel.Databases.ToList().ForEach(d => d.Dispose());
        }

        private DatabaseViewModel ConvertSelectionChangedToDatabaseViewModel(SelectionChangedEventArgs args)
        {
            if (args.AddedItems.Count == 0)
            {
                return null;
            }

            return args.AddedItems[0] as DatabaseViewModel;
        }

        private async Task<string> GetFolderPath(string title = "Open Folder")
        {
            var app = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            var window = app.MainWindow;
            var fileDialog = new OpenFolderDialog()
            {
                
                Directory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                Title = title
            };
            
            var selection = await fileDialog.ShowAsync(window).ConfigureAwait(false);
            return selection;
        }

        private async Task<string> GetFilepath(string title = "Open Folder", string extension = "")
        {
            var app = Application.Current.ApplicationLifetime as IClassicDesktopStyleApplicationLifetime;
            var window = app.MainWindow;
            var fileDialog = new OpenFileDialog()
            {
                
                Directory = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile),
                Title = title,
                AllowMultiple = false,
            };

            if (!string.IsNullOrWhiteSpace(extension))
            {
                fileDialog.Filters.Add(new FileDialogFilter
                {
                    Name = extension,
                    Extensions = new List<string>{extension}
                });
            }
            
            var selection = await fileDialog.ShowAsync(window).ConfigureAwait(false);
            if (selection == null || selection.Length == 0)
            {
                return "";
            }

            return selection[0];
        }

        private async void OnDbCloseButtonOnSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            var dbToRemove = ConvertSelectionChangedToDatabaseViewModel(args);
            if (dbToRemove == null)
            {
                return;
            }

            var result =
                await DialogBoxService.ShowOkCancelDialogBox("Confirm",
                    $"Are you sure you want to close {dbToRemove.Title}");
            if (result == ButtonResult.Ok)
            {
                DatabaseViewModelService.CloseDatabase(dbToRemove);
            }
            else
            {
                _dbCloseButton.SelectedIndex = -1;
            }
        }

        private async void OnDbExportButtonOnSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            _dbExportButton.SelectedIndex = -1;
            var dbToExport = ConvertSelectionChangedToDatabaseViewModel(args);
            if (dbToExport == null)
            {
                return;
            }

            var directory = await GetFolderPath("Database Export Folder");
            if (string.IsNullOrWhiteSpace(directory))
            {
                return;
            }
                
            var (success, files) = await DatabaseViewModelService.ExportDatabase(dbToExport, directory);
            var message = success ? "All tables exported" : "There was an error generating some tables...";
            var filesMessage = string.Join(Environment.NewLine, files);
            await DialogBoxService.ShowErrorDialog("Results", $"{message}{Environment.NewLine}{filesMessage}");
        }

        private async void OnDbImportButtonOnSelectionChanged(object sender, SelectionChangedEventArgs args)
        {
            _dbImportButton.SelectedIndex = -1;
            var dbToImportTo = ConvertSelectionChangedToDatabaseViewModel(args);
            if (dbToImportTo == null)
            {
                return;
            }

            var jsonFile = await GetFilepath("JSON to import", "json");
            if (string.IsNullOrWhiteSpace(jsonFile))
            {
                return;
            }

            if (!File.Exists(jsonFile))
            {
                await DialogBoxService.ShowErrorDialog("File Error",
                    $"Requested file does not exist or is not readable, cancelling: {jsonFile}");
                return;
            }

            var mainWindowVm = DataContext as MainWindowViewModel;
            var queryViewModel = mainWindowVm?.SelectedQueryViewModel;
            if (queryViewModel == null)
            {
                await DialogBoxService.ShowErrorDialog("Error",
                    "Error getting main query window data, can't continue with import");
                return;
            }

            queryViewModel.QueryText = "SELECT $ INTO <collection_name_for_import>" + Environment.NewLine +
                $"FROM $file_json('{jsonFile}');";
            queryViewModel.SelectedDatabaseViewModel = dbToImportTo;
        }

    }
}