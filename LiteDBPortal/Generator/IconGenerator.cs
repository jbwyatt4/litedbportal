using System;

using Avalonia;
using Avalonia.Media.Imaging;
using Avalonia.Platform;


namespace LiteDBPortal.Generator
{
    public static class IconGenerator
    {
        private static readonly string IconsBase = "avares://LiteDBPortal/Assets/Icons";
        
        public static readonly Bitmap DatabaseIcon = GetBitmap($"{IconsBase}/database.png");
        public static readonly Bitmap DatabaseSettingsIcon = GetBitmap($"{IconsBase}/database-settings.png");
        public static readonly Bitmap SystemSettingsIcon = GetBitmap($"{IconsBase}/file-settings-variant.png");
        public static readonly Bitmap ErrorIcon = GetBitmap($"{IconsBase}/alert-circle.png");
        public static readonly Bitmap FolderIcon =  GetBitmap($"{IconsBase}/folder.png");
        public static readonly Bitmap HelpIcon =  GetBitmap($"{IconsBase}/help-circle.png");
        public static readonly Bitmap InfoIcon =  GetBitmap($"{IconsBase}/information.png");
        public static readonly Bitmap TableIcon = GetBitmap($"{IconsBase}/table.png");
        public static readonly Bitmap TableSearchIcon = GetBitmap($"{IconsBase}/table-eye.png");
        public static readonly Bitmap TableEditIcon = GetBitmap($"{IconsBase}/table-plus.png");
        public static readonly Bitmap TableDeleteIcon = GetBitmap($"{IconsBase}/table-remove.png");
        public static readonly Bitmap TypeArray = GetBitmap($"{IconsBase}/code-array.png");
        public static readonly Bitmap TypeBoolean = GetBitmap($"{IconsBase}/ab-testing.png");
        public static readonly Bitmap TypeDateTime = GetBitmap($"{IconsBase}/calendar-clock.png");
        public static readonly Bitmap TypeDocument = GetBitmap($"{IconsBase}/json.png");
        public static readonly Bitmap TypeDecimal = GetBitmap($"{IconsBase}/decimal.png");
        public static readonly Bitmap TypeGuid = GetBitmap($"{IconsBase}/identifier.png");
        public static readonly Bitmap TypeMin = GetBitmap($"{IconsBase}/format-vertical-align-bottom.png");
        public static readonly Bitmap TypeMax = GetBitmap($"{IconsBase}/format-vertical-align-top.png");
        public static readonly Bitmap TypeNull = GetBitmap($"{IconsBase}/null.png");
        public static readonly Bitmap TypeNumber = GetBitmap($"{IconsBase}/numeric.png");
        public static readonly Bitmap TypeString = GetBitmap($"{IconsBase}/code-string.png");

        private static Bitmap GetBitmap(string path)
        {
            var locator = AvaloniaLocator.Current.GetService<IAssetLoader>();
            var uri = new Uri(path, UriKind.RelativeOrAbsolute);
            return new Bitmap(locator.Open(uri));
        }
        
    }
}