using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Avalonia.Media.Imaging;
using LiteDB;
using LiteDBPortal.ViewModels;

namespace LiteDBPortal.Generator
{
    public class QueryResultGenerator
    {
        private const string EllispsesString = "{...}";

        public static string GetJson(IEnumerable<BsonValue> bsonValues, int initialIndex = 0, bool writeIndex = true)
        {
            var sb = new StringBuilder();
            using var stringWriter = new StringWriter(sb);
            var jsonWriter = new JsonWriter(stringWriter)
            {
                Pretty = true
            };
            
            var currentIndex = initialIndex;
            
            foreach (var bson in bsonValues)
            {
                if (writeIndex)
                {
                    sb.AppendLine($"/* {currentIndex++}  */");
                }
                
                jsonWriter.Serialize(bson);
                sb.AppendLine();
            }

            return sb.ToString();
        }
        
        public static string GetJson(BsonDocument bsonDocument)
        {
            var sb = new StringBuilder();
            using var stringWriter = new StringWriter(sb);
            var jsonWriter = new JsonWriter(stringWriter)
            {
                Pretty = true
            };

            jsonWriter.Serialize(bsonDocument);
            sb.AppendLine();

            return sb.ToString();
        }


        public static List<QueryResultsTreeNodeModel> GetTreeViewNode(IEnumerable<BsonValue> bsonValues, int initialIndex = 0)
        {
            var currentIndex = initialIndex;
            var treeNodes = new List<QueryResultsTreeNodeModel>();

            foreach (var bson in bsonValues)
            {
                var label = bson.Type == BsonType.Document
                    ? $"({currentIndex++}){EllispsesString}"
                    : ConvertBsonToString(bson, "");
                var newNode = new QueryResultsTreeNodeModel(label, ConvertBsonTypeToBitmap(bson), bson);
                treeNodes.Add(newNode);
                if (bson.Type == BsonType.Document)
                {
                    PopulateNode(bson.AsDocument, newNode, bson.AsDocument);
                }

            }

            return treeNodes;
        }

        private static void PopulateNode(BsonDocument document, TreeNodeViewModel node, BsonDocument rootBson)
        {
            foreach (var key in document.Keys)
            {
                var bson = document[key];
                var text = ConvertBsonToString(bson, key);
                var newNode = new QueryResultsTreeNodeModel(text, ConvertBsonTypeToBitmap(bson), rootBson);
                node.Children.Add(newNode);
                if (bson.Type == BsonType.Document)
                {
                    PopulateNode(bson.AsDocument, newNode, rootBson);
                    continue;
                }

                if (bson.Type == BsonType.Array)
                {
                    PopulateNode(bson.AsArray, newNode, rootBson);
                }
            }
        }

        private static void PopulateNode(BsonArray document, TreeNodeViewModel node, BsonDocument rootBson)
        {
            var count = 0;
            foreach (var value in document)
            {
                var text = ConvertBsonToString(value, $"({count++})");
                var newNode = new QueryResultsTreeNodeModel(text, ConvertBsonTypeToBitmap(value), rootBson);
                node.Children.Add(newNode);
                if (value.Type == BsonType.Document)
                {
                    PopulateNode(value.AsDocument, newNode, rootBson);
                }
                
                if (value.Type == BsonType.Array)
                {
                    PopulateNode(value.AsArray, newNode, rootBson);
                }

            }
        }

        private static string ConvertBsonToString(BsonValue bson, string key)
        {
            if (bson.Type == BsonType.Document)
            {
                return $"{key}{EllispsesString} <{bson.Type}>";
            }

            if (bson.Type == BsonType.Array)
            {
                var count = bson.AsArray.Count;
                return $"{key}[{count}] <{bson.Type}>";
            }

            return $"{key}:{bson.ToString()} <{bson.Type}>";
        }

        private static Bitmap ConvertBsonTypeToBitmap(BsonType type)
        {
            switch (type)
            {
                case BsonType.Array:
                    return IconGenerator.TypeArray;
                case BsonType.Binary:
                    return IconGenerator.TypeNumber;
                case BsonType.Boolean:
                    return IconGenerator.TypeBoolean;
                case BsonType.Decimal:
                    return IconGenerator.TypeDecimal;
                case BsonType.Document:
                    return IconGenerator.TypeDocument;
                case BsonType.Double:
                    return IconGenerator.TypeDecimal;
                case BsonType.Guid:
                    return IconGenerator.TypeGuid;
                case BsonType.Int32:
                case BsonType.Int64:
                    return IconGenerator.TypeNumber;
                case BsonType.Null:
                    return IconGenerator.TypeNull;
                case BsonType.String:
                    return IconGenerator.TypeString;
                case BsonType.DateTime:
                    return IconGenerator.TypeDateTime;
                case BsonType.MaxValue:
                    return IconGenerator.TypeMax;
                case BsonType.MinValue:
                    return IconGenerator.TypeMin;
                case BsonType.ObjectId:
                    return IconGenerator.TypeGuid;
                default:
                    return IconGenerator.TypeNumber;
            }
        }
        
        private static Bitmap ConvertBsonTypeToBitmap(BsonValue value)
        {
            return ConvertBsonTypeToBitmap(value.Type);
        }

    }
}