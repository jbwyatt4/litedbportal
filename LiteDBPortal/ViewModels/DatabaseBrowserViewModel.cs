using System.Collections.ObjectModel;
using ReactiveUI;

namespace LiteDBPortal.ViewModels
{
    public class DatabaseBrowserViewModel :ViewModelBase
    {
        private TreeNodeViewModel _currentNode;

        public TreeNodeViewModel CurrentNode
        {
            get => _currentNode;
            set => this.RaiseAndSetIfChanged(ref _currentNode, value);
        }


        public ObservableCollection<DatabaseViewModel> Databases => QueryManagementService.Instance.Databases;

        public DatabaseBrowserViewModel()
        {
            
        }
    }
}