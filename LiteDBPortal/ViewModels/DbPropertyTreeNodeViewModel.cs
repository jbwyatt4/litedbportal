using System;
using System.Collections.Generic;
using Avalonia.Controls;
using Avalonia.Interactivity;
using Avalonia.Media.Imaging;
using ReactiveUI;

namespace LiteDBPortal.ViewModels
{
    public class DbPropertyTreeNodeViewModel : TreeNodeViewModel
    {
        private bool _hasSelectMenu;
        private bool _hasDistinctMenu;
        private bool _hasCountMenu;
        private bool _hasInsertMenu;
        private bool _hasDeleteMenu;
        private bool _hasExplainMenu;
        private bool _hasIndexesMenu;
        private bool _hasRenameMenu;
        private bool _hasDropCollectionMenu;
        private bool _hasMenus;
        
        public ViewModelBase ParentViewModel { get; set; }

        public bool HasSelectMenu
        {
            get => _hasSelectMenu;
            set => this.RaiseAndSetIfChanged(ref _hasSelectMenu, value);
        }

        public bool HasDistinctMenu
        {
            get => _hasDistinctMenu;
            set => this.RaiseAndSetIfChanged(ref _hasDistinctMenu, value);
        }

        public bool HasCountMenu
        {
            get => _hasCountMenu;
            set => this.RaiseAndSetIfChanged(ref _hasCountMenu, value);
        }

        public bool HasInsertMenu
        {
            get => _hasInsertMenu;
            set => this.RaiseAndSetIfChanged(ref _hasInsertMenu, value);
        }

        public bool HasDeleteMenu
        {
            get => _hasDeleteMenu;
            set => this.RaiseAndSetIfChanged(ref _hasDeleteMenu, value);
        }

        public bool HasExplainMenu
        {
            get => _hasExplainMenu;
            set => this.RaiseAndSetIfChanged(ref _hasExplainMenu, value);
        }

        public bool HasIndexesMenu
        {
            get => _hasIndexesMenu;
            set => this.RaiseAndSetIfChanged(ref _hasIndexesMenu, value);
        }

        public bool HasRenameMenu
        {
            get => _hasRenameMenu;
            set => this.RaiseAndSetIfChanged(ref _hasRenameMenu, value);
        }

        public bool HasDropCollectionMenu
        {
            get => _hasDropCollectionMenu;
            set => this.RaiseAndSetIfChanged(ref _hasDropCollectionMenu, value);
        }
        
        public bool HasMenus
        {
            get => _hasMenus;
            set => this.RaiseAndSetIfChanged(ref _hasMenus, value);
        }
        
        public DbPropertyTreeNodeViewModel(string text, Bitmap icon, object nativeData = default) : base(text, icon, nativeData)
        {
            HasMenus = true; //TODO remove once hidden ContextMenus render correctly
        }

        public void DoSelectQuery()
        {
            string cmd = $"SELECT $ FROM {Text};";
            SendQueryToViewModel(cmd);
        }

        public void DoDistinctQuery()
        {
            string cmd = $"SELECT DISTINCT(*.<desired field here>) FROM {Text};";
            SendQueryToViewModel(cmd);
        }
        
        public void DoCountQuery()
        {
            string cmd = $"SELECT COUNT(*) FROM {Text};";
            SendQueryToViewModel(cmd);
        }

        public void DoDropQuery()
        {
            string cmd = $"DROP COLLECTION {Text}";
            SendQueryToViewModel(cmd);
           
        }

        public void DoExplainQuery()
        {
            string cmd = $"EXPLAIN SELECT $ FROM {Text};";
            SendQueryToViewModel(cmd);
        }

        public void DoIndexesQuery()
        {
            string cmd = $"SELECT $ FROM $indexes WHERE collection = \"{Text}\";";
            SendQueryToViewModel(cmd);
        }

        public void DoInsertQuery()
        {
            string cmd = $"INSERT INTO {Text} VALUES " +
                         "{<JSON TO Insert Here>}" +
                         ";";
            SendQueryToViewModel(cmd);
        }
        
        public void DoRenameQuery()
        {
            string cmd = $"RENAME COLLECTION {Text} TO <New Collection Name>";
            SendQueryToViewModel(cmd);
           
        }


        private void SendQueryToViewModel(string queryCommand)
        {
            var currentQuery = QueryManagementService.Instance.CurrentQuery;
            currentQuery.SelectedDatabaseViewModel = ParentViewModel as DatabaseViewModel;
            currentQuery.QueryText = queryCommand;
        }
    }
}