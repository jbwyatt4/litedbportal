# LiteDB Portal

A [LiteDB](https://www.litedb.org/) browser akin to LiteDB.Studio or Robomongo 
but written in [Avalonia](http://avaloniaui.net/) so 
useful on Linux, Mac, and Windows.

Read a lot more about the software in our [wiki](https://gitlab.com/HankG/litedbportal/wikis/home) for more info.

Checkout our [screenshots gallery](https://gitlab.com/HankG/litedbportal/wikis/Screenshots-Gallery) to see it on various platforms.

See our [Getting Started Tutorial](https://gitlab.com/HankG/litedbportal/wikis/Getting-Started-Tutorial) to try it out

Using the tool is as easy as pulling down a copy of the source here and simply typing `dotnet run`


![](LiteDBPortalScreenshot.png)